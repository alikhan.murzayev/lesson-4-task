package main

import (
	"fmt"
	"math"
)

type Shape interface {
	Area() float64
}

// Circle START
type Circle struct {
	R float64
}

func (c Circle) Area() float64 {
	return math.Pi * c.R * c.R / 2
}

// Circle END

// Square START
type Square struct {
	A float64
}

func (s Square) Area() float64 {
	return s.A * s.A
}

// Square END

// Rectangle START
type Rectangle struct {
	A, B float64
}

func (r Rectangle) Area() float64 {
	return r.A * r.B
}

// Rectangle END

// Triangle START
type Triangle struct {
	A, B, C float64
}

func (t Triangle) Area() float64 {
	p := (t.A + t.B + t.C) / 2
	return math.Sqrt(p * (p - t.A) * (p - t.B) * (p - t.C))

}

// Triangle END

func main() {
	var shape Shape

	var circleR float64 = 6
	shape = Circle{R: circleR}
	fmt.Printf("Area of circle with radius %f = %f\n", circleR, shape.Area())

	var squareA float64 = 10
	shape = Square{A: squareA}
	fmt.Printf("Area of square with side %f = %f\n", squareA, shape.Area())

	var rectangleA, rectangleB float64 = 4, 6
	shape = Rectangle{A: rectangleA, B: rectangleB}
	fmt.Printf("Area of rectangle with sides %f and %f = %f\n", rectangleA, rectangleB, shape.Area())

	var triangleA, triangleB, triangleC float64 = 10, 10, 10
	shape = Triangle{A: triangleA, B: triangleB, C: triangleC}
	fmt.Printf("Area of triangle with sides %f, %f and %f = %f\n", triangleA, triangleB, triangleC, shape.Area())

}
